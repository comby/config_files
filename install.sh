#!/bin/bash

if [ -f ~/.bash_aliases_local ]; then
  . ~/.bash_aliases_local
  shopt -s expand_aliases
fi

# Change dir to files location in repo, in case script was called from somewhere else
cd "$(dirname "$0")"

filesdir="$(pwd)/homedir/"
# Recursively create symlinks
# ln -vsf "$filesdir/.*" "$HOME"
find "$filesdir" -maxdepth 1 -mindepth 1| while read file; do ln -vsf "$file" "$HOME/"; done

ln -vsf "$(pwd)/homedir/.ssh/config" "$HOME/.ssh/config"
