#!/usr/bin/env bash
# .bashrc générique mis à votre disposition
# par les gentils membres actifs du Cr@ns
# Vous pouvez l'utiliser, le redistribuer, le modifier à votre convenance.
# Des questions, des suggestions : {nounou,ca}@lists.crans.org
# Licence : WTFPL

# Les sections commentées par #~# sont des features qui ne sont pas activées
# par défaut. Sentez-vous libre de les décommenter pour les utiliser.

#------------------------------------------------------------------------------

# Pour les shells non interactif (sous emacs, avec rcp, ...)
# on ne charge pas le .bashrc
[[ ( -z "$PS1")  || ("$TERM" = dumb) ]] && return

# Hack pour que $COLUMNS contienne le nombre de colonne du terminal
# Sinon, le prompt kikoo risque de déborder/être trop court
COLUMNS=$(tput cols)
shopt -s checkwinsize

# +-------------------------+
# | Customizations diverses |
# +-------------------------+

# Utilisation de lesspipe comme PAGER si il est présent
[[ -x /usr/bin/lesspipe ]] && eval "$(lesspipe)"

# Couleur pour certaines commandes (ls, …)
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# Bash-complétion
[ -f /etc/bash_completion ] && . /etc/bash_completion
# Placez vos fichiers de bash-complétion custom dans ~/.bash_completion.d/
# ils seront chargés par la ligne suivante
[ -d ~/.bash_completion.d/ ] && for f in ~/.bash_completion.d/*; do source "$f"; done

# +-----+
# | GIT |
# +-----+
# Checks if the command provided is in the commands list and is
# executable
check_command(){
    [[ -n ${commands[$1]} ]] && [ -x "${commands[$1]}" ] && return 0
    return 1
}

# If name should be overwritten (eg for git-svn), do it.
vcs_adjust(){
    [[ -n ${vcs_comm[overwrite_name]} ]] && vcs=${vcs_comm[overwrite_name]}
    return 0
}


# Formats two VCS_info messages, one with colors, and one without
vcs_formats(){
    local action=$1 branch=$2 base=$3 rev=$4
    local msg
    local -i i

    # printf is for readability (it's easier to find %s)
    msg="${MINUS_CHAR}[%s/%s"
    msg=$(printf $msg ${base/*\/} $branch)

    # If there is a revnumber, print it
    if [ ! -z ${rev} ]; then
        msg="${msg}:%s"
        msg=$(printf $msg $rev)
    fi

    # Print the current cvs action state
    if [ ! -z ${action} ] ; then
        msg="${msg}|%s"
        msg=$(printf $msg $action)
    fi
    msg="${msg}]${MINUS_CHAR}"

    msgs[1]=$msg

    # Same shit with colors
    msg="${nocolor_prompt}${vcs_symbols_color}[${vcs_repo_color}%s${vcs_sep_color}/${vcs_branch_color}%s"
    msg=$(printf $msg ${base/*\/} $branch)
    if [[ -n ${rev} ]] ; then
        msg="${msg}${vcs_colon_color}:${vcs_rev_color}${rev}"
    fi
    if [[ -n ${action} ]] ; then
        msg="${msg}${nocolor_prompt}|${vcs_action_color}${action}"
    fi
    msg="${msg}${vcs_symbols_color}]${line_color_prompt}${MINUS_CHAR}"
    msgs[0]=$msg

    return 0
}

# Uses -P option for cd in order to resolve symlinks
vcs_realpath(){
    (
        cd -P "$1" 2>/dev/null && pwd
    )
}

# Feature to detect a special dir, at the top of
# the current repo
detect_by_dir(){
    local dirname=$1
    local basedir="." realbasedir

    realbasedir="$(vcs_realpath ${basedir})"
    while [[ ${realbasedir} != '/' ]]; do
        [[ -r ${realbasedir} ]] || return 1

        # Tries to find detect_need_file (eg formats) in the dir
        if [[ -n ${vcs_comm[detect_need_file]} ]] ; then
            [[ -d ${basedir}/${dirname} ]] && \
            [[ -e ${basedir}/${dirname}/${vcs_comm[detect_need_file]} ]] && \
                break
        else
            [[ -d ${basedir}/${dirname} ]] && break
        fi

        basedir=${basedir}/..
        realbasedir="$(vcs_realpath ${basedir})"
    done

    [[ ${realbasedir} == "/" ]] && return 1
    vcs_comm[basedir]=${realbasedir}
    return 0
}

# Git is powerfull
git_detect(){
    if check_command git && git rev-parse --is-inside-work-tree &> /dev/null; then
        vcs_comm[gitdir]="$(git rev-parse --git-dir 2> /dev/null)" || return 1
        if   [[ -d ${vcs_comm[gitdir]}/svn ]]             ; then vcs_comm[overwrite_name]='git-svn'
        elif [[ -d ${vcs_comm[gitdir]}/refs/remotes/p4 ]] ; then vcs_comm[overwrite_name]='git-p4' ; fi
        return 0
    fi
    return 1
}

# Find git's branch
git_getbranch (){
    local gitbranch gitdir=$1 tmp actiondir
    local gitsymref='git symbolic-ref HEAD'

    # In certain circumstances, we have to take into account
    # actions
    actiondir=''
    for tmp in "${gitdir}/rebase-apply" \
               "${gitdir}/rebase"       \
               "${gitdir}/../.dotest"; do
        if [[ -d ${tmp} ]]; then
            actiondir=${tmp}
            break
        fi
    done
    if [[ -n ${actiondir} ]]; then
        gitbranch="$(${gitsymref} 2> /dev/null)"
        [[ -z ${gitbranch} ]] && [[ -r ${actiondir}/head-name ]] \
            && gitbranch="$(< "${actiondir}"/head-name)"

    # MERGE_HEAD state
    elif [[ -f "${gitdir}/MERGE_HEAD" ]] ; then
        gitbranch="$(eval $gitsymref 2> /dev/null)"
        [[ -z ${gitbranch} ]] && gitbranch="$(< ${gitdir}/MERGE_HEAD)"

    # rebase
    elif [[ -d "${gitdir}/rebase-merge" ]] ; then
        gitbranch="$(< ${gitdir}/rebase-merge/head-name)"

    # dotest
    elif [[ -d "${gitdir}/.dotest-merge" ]] ; then
        gitbranch="$(< ${gitdir}/.dotest-merge/head-name)"

    # Normal case
    else
        gitbranch="$(eval $gitsymref 2> /dev/null)"

        # shit happens
        if [[ $? -ne 0 ]] ; then
            gitbranch="refs/tags/$(git describe --exact-match HEAD 2>/dev/null)"

            # big shit happens
            if [[ $? -ne 0 ]] ; then
                gitbranch=$(< $gitdir/HEAD)
                gitbranch="${gitbranch:0:7}..."
            fi
        fi
    fi

    # keep only the last part of gitbranch
    printf '%s' "${gitbranch#refs/[^/]*/}"
    return 0
}

git_getaction(){
    local gitaction='' gitdir=$1
    local tmp

    for tmp in "${gitdir}/rebase-apply" \
               "${gitdir}/rebase"       \
               "${gitdir}/../.dotest" ; do
        if [[ -d ${tmp} ]] ; then
            if   [[ -f "${tmp}/rebasing" ]] ; then
                gitaction="rebase"
            elif [[ -f "${tmp}/applying" ]] ; then
                gitaction="am"
            else
                gitaction="am/rebase"
            fi
            printf '%s' ${gitaction}
            return 0
        fi
    done

    for tmp in "${gitdir}/rebase-merge/interactive" \
               "${gitdir}/.dotest-merge/interactive" ; do
        if [[ -f "${tmp}" ]] ; then
            printf '%s' "rebase-i"
            return 0
        fi
    done

    for tmp in "${gitdir}/rebase-merge" \
               "${gitdir}/.dotest-merge" ; do
        if [[ -d "${tmp}" ]] ; then
            printf '%s' "rebase-m"
            return 0
        fi
    done

    if [[ -f "${gitdir}/MERGE_HEAD" ]] ; then
        printf '%s' "merge"
        return 0
    fi

    if [[ -f "${gitdir}/BISECT_LOG" ]] ; then
        printf '%s' "bisect"
        return 0
    fi
    return 1
}

git_get_data(){
    local gitdir gitbase gitbranch gitaction

    gitdir=${vcs_comm[gitdir]}
    gitbranch="$(git_getbranch ${gitdir})"

    if [[ -z ${gitdir} ]] || [[ -z ${gitbranch} ]] ; then
        return 1
    fi

    vcs_adjust
    gitaction="$(git_getaction ${gitdir})"
    gitprefix=$(git rev-parse --show-prefix)
    gitbase=${PWD%/${gitprefix%/}}
    vcs_formats "${gitaction}" "${gitbranch}" "${gitbase}" ''
    return 0
}


vcs_info(){
    local -i found
    local -ax msgs
    local -Ax vcs_comm commands
    local -x vcs
    local -a vcss
    local -A disabled

    vcs="init"
    vcss=(git)
    disabled[cdv]=1
    disabled[cvs]=1
    disabled[bzr]=1
    for i in $(seq 0 $(( ${#vcss[*]} - 1 ))); do
        if [[ disabled[${vcss[$i]}] -eq 1 ]]; then
            continue
        fi
        commands[${vcss[$i]}]=$( (which ${vcss[$i]} 2>/dev/null >&2 && which ${vcss[i]})||echo true);
    done;

    found=0
    for vcs in ${vcss[*]}; do
        if [[ disabled[${vcs}] -eq 1 ]]; then
            continue
        fi
        ${vcs}_detect && found=1 && break
    done

    (( found == 1 )) && ${vcs}_get_data

    if [ ${color_prompt} = "yes" ]; then
        VCS_info=${msgs[0]}
    else
        VCS_info=${msgs[1]}
    fi
    VCS_size=${#msgs[1]}
}


### Attention à ce que vous éditez dans cette section ###
### v v v v v v v v v v ###
# +----------------------+
# | Génération du prompt |
# +----------------------+

nocolor='\e[0m'

cyan='\e[1;36m'
cyan_thin='\e[0;36m'
violet='\e[1;35m'
violet_thin='\e[0;35m'
jaune='\e[1;33m'
jaune_thin='\e[0;33m'
rouge='\e[1;31m'
rouge_thin='\e[0;31m'
vert='\e[1;32m'
vert_thin='\e[0;32m'
bleu='\e[1;34m'
bleu_thin='\e[0;34m'
blanc='\e[1;37m'
blanc_thin='\e[0;37m'
nocolor='\e[0m'
cyan_prompt="\[${cyan}\]"
cyan_thin_prompt="\[${cyan_thin}\]"
violet_prompt="\[${violet}\]"
violet_thin_prompt="\[${violet_thin}\]"
jaune_prompt="\[${jaune}\]"
jaune_thin_prompt="\[${jaune_thin}\]"
rouge_prompt="\[${rouge}\]"
rouge_thin_prompt="\[${rouge_thin}\]"
vert_prompt="\[${vert}\]"
vert_thin_prompt="\[${vert_thin}\]"
bleu_prompt="\[${bleu}\]"
bleu_thin_prompt="\[${bleu_thin}\]"
blanc_prompt="\[${blanc}\]"
blanc_thin_prompt="\[${blanc_thin}\]"
nocolor_prompt="\[${nocolor}\]"


# Est-ce qu'on veut que le prompt affiche les information sur l'éventuel dépôt
# versionné dans lequel on se trouve
#~# # Changez cette variable en "yes" pour l'afficher
display_vcs_info=yes


# Génération de la ligne de "-"
function gen_minus_line
{
    local i

    MINUS_LINE=""
    # Pas la peine de refaire le travail tant qu'on ne change
    # pas le nombre de colonnes.
    SAVE_COLUMNS=$COLUMNS
    for ((i = COLUMNS-23; i>0; i--)); do
        MINUS_LINE=$MINUS_CHAR$MINUS_LINE
    done
}
# Génération du prompt après chaque commande
function prompt_command
{
    local pwd ERR DATE PROMPT DIR POST_DIR
    # Cette fonction génère un prompt sur deux lignes
    # ayant cette allure :
    # ─( 12:59:05 )─< ~ >───────────────────────────────────────────────────────[ 0 ]─
    # user@host $

    # Attention aux hacks (échappement, interprétation des variables)
    # pour que les couleurs (et leurs tailles)
    # soient évaluées au moment de l'affichage du prompt
    # Si le terminal a été redimensionné, on régénère la ligne de tirets
    (( SAVE_COLUMNS == COLUMNS )) || gen_minus_line

    # À décommenter si on veut afficher des infos
    # quand on se trouve dans un dépôt versionné
    if [ "$display_vcs_info" = yes ]; then
        vcs_info
    fi

    # Chemin courant, en faisant attention à la largeur de la fenêtre
    pwd=${PWD/#$HOME/'~'}
    if (( ${#pwd} + ${VCS_size} + 27 > COLUMNS )); then
 	    if (( COLUMNS >= 33 )); then
	        DIR='..'${pwd:${#pwd}+${VCS_size}+30-COLUMNS}
            POST_DIR='${MINUS_LINE:0:4-${#?}}'
	    else
	        DIR=$pwd
            POST_DIR=$MINUS_CHAR
	    fi
    else
	    DIR=$pwd
        POST_DIR='${MINUS_LINE:'${#pwd}+${VCS_size}+1'+${#?}}'
    fi

    if [[ $VIRTUAL_ENV != "" ]];then
        VENV="(p:${VIRTUAL_ENV##*/})"
    elif [[ $CONDA_PROMPT_MODIFIER != "" ]]; then
         VENV="(c:${CONDA_PROMPT_MODIFIER:1:-2})"
    else
        VENV=''
    fi
    if [[ ${STY} != "" ]]
    then
        SCREEN_NAME=${STY//[[:digit:]]/}
        SCREEN_ENV="[${SCREEN_NAME#.}]"
    fi

    # Génération des différents morceaux du prompt, avec ou sans la couleur
    if [ "$color_prompt" = yes ]; then
        DIR="< ${pwd_color}$DIR${line_color_prompt} >"
        # On affiche le code de retour en "no color" si c'est 0, en rouge sinon
        # Ceci n'est pas customizable à coup de variable,
        # il faut changer $((31+($?==0)*6)) si vous y tenez vraiment et que vous comprenez ce que vous faites
        ERR=$'[ \[\e[1;$((31+($?==0)*6))m\]$?'${line_color_prompt}' ]'$MINUS_CHAR
        # Affichage de l'heure courante
        # pratique pour savoir quand on a tapé la commande précédente
        DATE="( ${date_color}\D{%H:%M:%S}${line_color_prompt} )"
        # Deuxième ligne du prompt "user@host $"
        # Attention "\\\$" devient \$, c'est-à-dire # pour root, $ pour les autres
        PROMPT="${username_color_prompt}\u${symbols_color_prompt}@${host_color_prompt}\h ${symbols_color_prompt}${SCREEN_ENV}${VENV}${symbols_color_prompt}\\\$ ${nocolor_prompt}"
        # On fusionne tout ça
        PS1=$TITLE${line_color_prompt}$MINUS_CHAR$DATE$MINUS_CHAR$DIR$MINUS_CHAR${line_color_prompt}$MINUS_CHAR$VCS_info$ERR'\n'$PROMPT
    else
        DIR="< "$DIR" >"
        ERR='[ $? ]'$MINUS_CHAR
        DATE="( \D{%H:%M:%S} )"
        PROMPT="\u@\h \\\$ "
        PS1=$TITLE$MINUS_CHAR$DATE$MINUS_CHAR$DIR$MINUS_CHAR$POST_DIR$VCS_info$ERR'\n'$PROMPT
    fi
}

username_color=$blanc
if [[ "$USER" = pac   ]]; then username_color=$jaune; fi
if [[ "$USER" = comby ]]; then username_color=$vert; fi
if [[ "$USER" = pi    ]]; then username_color=$jaune; fi
if [[ "$USER" = root  ]]; then username_color='\e[6m\e31m';  fi

host_color=none
# crans
if [[ "$HOST" = zamok      ]]; then host_color=$cyan;  fi
if [[ "$HOST" = thot       ]]; then host_color=$vert;  fi
if [[ "$HOST" = redisdead  ]]; then host_color=$rouge; fi
if [[ "$HOST" = silice     ]]; then host_color=$jaune; fi
if [[ "$HOST" = kiwi       ]]; then host_color=$vert;  fi
if [[ "$HOST" = bde-note   ]]; then host_color=$rouge; fi
if [[ "$HOST" = sauron     ]]; then host_color=$rouge; fi
if [[ "$host_color" = none ]]; then
    host_color="\e[38;5;$(($(head -1 /etc/hostname| md5sum | sed "s/[a-f]//g" | head -c 3) % 210+ 19))m"
fi

if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # support de la couleur
    color_prompt=yes
    # Couleurs dans "user@host $"
    username_color_prompt="\[${username_color}\]"
    host_color_prompt="\[${host_color}\]"
    symbols_color_prompt=${blanc_prompt}
    # Couleur de la ligne séparatrice de prompt
    line_color=${host_color}
#    line_color_prompt=${cyan_prompt}
    line_color_prompt=${host_color_prompt}
    # Couleur du path actuel
    pwd_color=${jaune_prompt}
    # Couleur de la date (à chaque affichage du prompt)
    date_color=${violet_prompt}
    # Couleur de la date au premier affichage (à l'ouverture du terminal)
    announce_date_color=${blanc}
    # Couleur d'affichage de vcs_info
    vcs_symbols_color=${violet_thin_prompt}
    vcs_type_color=${jaune_thin_prompt}
    vcs_branch_color=${vert_thin_prompt}
    vcs_repo_color=${vert_thin_prompt}
    vcs_action_color=${rouge_thin_prompt}
    vcs_sep_color=${jaune_thin_prompt}
    vcs_rev_color=${jaune_thin_prompt}
    vcs_colon_color=${rouge_thin_prompt}
else
    # pas de support de la couleur
    color_prompt=no
fi

# On régénére le prompt après chaque commande
PROMPT_COMMAND=prompt_command

PROMPT_COMMAND="history -a; $PROMPT_COMMAND"

# +-------------------+
# | Messages au début |
# +-------------------+

function welcome_message ()
{
    # Si on supporte l'utf-8 et qu'on n'est pas dans un screen,
    # on affiche un joli cadre avec la date.
    # On utilise aussi un caractère plus sympa pour faire les lignes de tirets
    if [[ $(uname) == Linux && ( $(locale charmap) == UTF-8 && $TERM != screen ) ]]; then
        MINUS_CHAR=─
        gen_minus_line
        date=$(/bin/date +"%R, %A %d %B %Y")
        # ${redate} sert à connaître la taille de la chaine $date,
        # mais sans se planter sur le nombre d'octet/nombre de caractères
        redate=${date//é/e}
        redate=${redate//û/u}
        if [ "$color_prompt" = yes ]; then
            echo -e "${line_color}┬─${redate//?/─}─┬${MINUS_LINE:${#date}-19}\n│ ${announce_date_color}$date ${line_color}│\n└─${redate//?/─}─┘\e[0m\n"
        else
            echo -e "┬─${redate//?/─}──┬${MINUS_LINE:${#date}-18}\n│ $date  │\n└─${redate//?/─}──┘\n"
        fi
        unset date
    # Sinon, on se contente d'une ligne de tirets
    else
        MINUS_CHAR=-
        gen_minus_line
    fi
}
### ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ###
### Attention à ce que vous avez édité dans cette section ###





# +--------------------+
# | Alias et fonctions |
# +--------------------+
#

function sshzachs(){
    ssh pc28068@is245233.intra.cea.fr 'rm -rf /run/user/280686/gnupg/S.gpg-agent'
    ssh zachs
}

function jpt() {
    port=${2:-8888}
    ssh -L ${port}:localhost:${port} $1 -o RemoteCommand=None "source /home/pc280686/.bashrc &&  /home/pc280686/.local/bin/jupyter-lab --no-browser --port=${port}"
}

# +---------------------------------------+
# | Gestion de l'historique des commandes |
# +---------------------------------------+

# On spécifie explicitement le fichier d'historique.
# En effet, si on jongle entre bash et zsh, per exemple,
# comme ils n'utilisent pas le même format pour stocker le timestamp
# des commandes, on se retrouve avec des comportements non souhaités
# si on ne change pas le fichier d'historique entre les deux.
export HISTFILE=~/.bash_history

# On ne sauve pas les lignes dupliquées qui se suivent dans l'historique
# ainsi que les commandes qui commencent par une espace
export HISTCONTROL=ignoreboth

# Nombre de lignes d'historique à garder
export HISTSIZE=5000

# la même chose dans le fichier d'historique
export HISTFILESIZE=5000

# Mémoriser le timestamp d'exécution de la commande
export HISTTIMEFORMAT='%F %T - '

# pour sauvegarder les commandes de plusieurs lignes (\ ) en une seule
shopt -s cmdhist
# pour enregistrer tout de suite les commandes dans l'historique
# et non pas à la fermeture du shell
shopt -s histappend


# +---------------+
# | Environnement |
# +---------------+

# Pour éviter un troll, aucune ligne de cette section n'est décommentée.
# Choisissez la vôtre.
#~# export EDITOR='/usr/bin/nano'
#~# export EDITOR='/usr/bin/vim'
#~# export EDITOR='/usr/bin/jed'
export EDITOR='/usr/bin/vim'




#~# # Décommentez les lignes suivantes et peuplez-les correctement si
#~# # vous avez besoin de commiter en tant que root *et* que votre
#~# # /etc/sudoers contient bien la directive
#~# # Defaults env_keep += "<les variables en question>"
#~# export GIT_AUTHOR_NAME="$(git config user.name)"
#~# export GIT_AUTHOR_EMAIL="$(git config user.email)"

# +----------+
# | Sourcing |
# +----------+

# Chargement des alias
[[ -f ~/.bash_aliases ]] && . ~/.bash_aliases

# Fichier locaux (utile quand on clone sa config sur plusieurs machines)
[[ -f ~/.bashrc.local ]] && . ~/.bashrc.local

[[ -f ~/.bash_aliases_local ]] && . ~/.bash_aliases_local



#export PYENV_ROOT="/home/$USER/.pyenv"
#export PATH="$PYENV_ROOT/bin:$PATH"
#if command -v pyenv 1>/dev/null 2>&1; then
  #  eval "$(pyenv init -)"
  #  eval "$(pyenv virtualenv-init -)"
#fi
PATH="/usr/local/texlive/2024/bin/x86_64-linux:$PATH"

# Ajouter ~/bin, /sbin et /usr/sbin à son PATH
PATH=~/.local/bin:~/bin:$PATH
PATH=$PATH:/root/.local/bin



function cudaswap() {

    if [[ -z "${LD_LIBRARY_PATH_CUDA_BACKUP}" ]]; then
        export LD_LIBRARY_PATH_CUDA_BACKUP=$LD_LIBRARY_PATH
    fi
    if [[ -z "${PATH_CUDA_BACKUP}" ]]; then
        export PATH_CUDA_BACKUP=$PATH
    fi

    if [[ $1 == "latest" ]]; then
        local cuda_folder="/usr/local/cuda"
        local package_name="cuda"
    elif [[ -n "$1" ]]; then
        local cuda_folder="/usr/local/cuda-$1"
        local package_name="cuda-toolkit-${1//./-}"
    else
        return 1
    fi

    local cuda_bin_path="$cuda_folder/bin"
    local cuda_lib_path="$cuda_folder/lib64"

    export PATH=$cuda_bin_path:$PATH_CUDA_BACKUP
    export LD_LIBRARY_PATH=$cuda_lib_path:$LD_LIBRARY_PATH_CUDA_BACKUP

    if command -v nvcc > /dev/null; then
        local nvcc_version=$(nvcc --version | grep release | awk '{print $6}' | cut -d ',' -f1 | sed 's/V//')
        if [[ -z "$1" ]] || [[ "$nvcc_version" == *"$1"* ]]; then
            echo "NVIDIA CUDA version $nvcc_version is now active."
        else
            echo "Warning: Active CUDA version ($nvcc_version) does not match the requested version ($1)."
        fi
    fi
}

CUDA_HOME=/usr/local/cuda
#cuda shit
[[ -d $CUDA_HOME ]]
cudaswap 12.4

export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
export LANG=en_US.utf8

export PASSWORD_STORE_ENABLE_EXTENSIONS=true
# +-----+
# | End |
# +-----+
welcome_message
# On n'exécute le welcome_message que à ce moment, parce que la customisation
# local a pu changer des couleursage



# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/volatile/home/pc280686/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/volatile/home/pc280686/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/volatile/home/pc280686/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/volatile/home/pc280686/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

